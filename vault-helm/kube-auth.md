```
# Configure vault client
export VAULT_ADDR=
export VAULT_TOKEN=

kubectl create serviceaccount vault-auth
kubectl apply --filename vault-auth-service-account.yml

export VAULT_SA_SECRET=$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=$(kubectl get secret $VAULT_SA_SECRET -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=$(kubectl get secret $VAULT_SA_SECRET -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)
export K8S_HOST=$(kubectl get endpoints kubernetes -o yaml | grep ip: | awk '{print $3;}')

export VAULT_SKIP_VERIFY=1
vault auth enable kubernetes
vault write auth/kubernetes/config \
    token_reviewer_jwt="$SA_JWT_TOKEN" \
    kubernetes_host="https://$K8S_HOST:443" \
    kubernetes_ca_cert="$SA_CA_CRT"
```

Testing
```
vault write auth/kubernetes/role/test \
        bound_service_account_names=test-sa \
        bound_service_account_namespaces=default \
        policies=read-creds \
        ttl=60m

```